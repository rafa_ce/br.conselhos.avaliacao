﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Utils
{
    public class ArquivoLog
    {
        private StreamWriter stream;

        public ArquivoLog(string path)
        {
            stream = File.CreateText(path);
        }

        public void Fechar()
        {
            stream.Close();
        }

        public void EscreverProcessoNaoEncontrado(string numero)
        {
            stream.WriteLine("{0} - Processo número {1} não encontado.",
                DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss"), numero);
        }

        public void EscreverFalhaConverterPDF(string numero)
        {
            stream.WriteLine("{0} - Não foi possível salvar o Documento em PDF do processo {1}.",
                DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss"), numero);
        }
    }
}
