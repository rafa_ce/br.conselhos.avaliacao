﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Utils
{
    public class PDFUtil
    {
        public static string ConverteTxtEmPDF(string arquivoTxt)
        {
            StreamReader reader = new StreamReader(arquivoTxt);
            
            Document doc = new Document();

            string arquivoPdf = (arquivoTxt).Replace(".txt", ".pdf");
            PdfWriter.GetInstance(doc, new FileStream(arquivoPdf, FileMode.Create));

            doc.Open();
            doc.Add(new Paragraph(reader.ReadToEnd()));
            doc.Close();

            return arquivoPdf;
        }
    }
}
