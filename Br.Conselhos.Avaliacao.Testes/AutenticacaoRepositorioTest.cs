﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Br.Conselhos.Avaliacao.Dados;
using Br.Conselhos.Avaliacao.Dados.Repositorio;
using Br.Conselhos.Avaliacao.Dados.Modelos;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace Br.Conselhos.Avaliacao.Testes
{
    [TestClass]
    public class AutenticacaoRepositorioTest
    {
        private UsuarioRepositorio repositorio;
        protected static Contexto contexto;
        protected DbContextTransaction transaction;

        [ClassInitialize()]
        public static void ClassSetup(TestContext a)
        {
            using (var contexto = new Contexto())
            {
                contexto.Database.CreateIfNotExists();
            }
        }

        [ClassCleanup]
        public static void ClassCleanUp()
        {
            using (var contexto = new Contexto())
            {
                if (contexto.Database.Exists())
                    contexto.Database.Delete();
            }
        }

        [TestInitialize]
        public void TransactionTestStart()
        {
            contexto = new Contexto();
            repositorio = new UsuarioRepositorio(contexto);
            transaction = contexto.Database.BeginTransaction();
        }

        [TestCleanup]
        public void TransactionTestEnd()
        {
            transaction.Rollback();
            transaction.Dispose();
            contexto.Dispose();
        }

        [TestMethod]
        public void CriarUsuario()
        {
            Usuario usuario = new Usuario("Admin");

            IdentityResult result = repositorio.CreateUser(usuario, "Admin1");
            contexto.SaveChanges();

            Assert.IsTrue(result.Succeeded);
        }
        
        [TestMethod]
        public void NaoCriarUsuarioComNomeRepetido()
        {
            Usuario usuario = new Usuario("Admin");

            IdentityResult result = repositorio.CreateUser(usuario, "Admin1");
            contexto.SaveChanges();

            Assert.IsTrue(result.Succeeded);

            usuario = new Usuario("Admin");

            result = repositorio.CreateUser(usuario, "Admin1");
            contexto.SaveChanges();

            Assert.IsFalse(result.Succeeded);
        }

        [TestMethod]
        public void NaoCriarUsuarioSenhaMenorQueSeisDigitos()
        {
            Usuario usuario = new Usuario("Admin");

            IdentityResult result = repositorio.CreateUser(usuario, "Admin");
            contexto.SaveChanges();

            Assert.IsFalse(result.Succeeded);
        }
    }
}
