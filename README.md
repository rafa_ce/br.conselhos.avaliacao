# Prova programação Rafael Contessotto #

A aplicação foi desenvolvida utilizando C#, Windows Forms e o banco de dados PostgreSQL.

### Autenticação ###
Para cadastro e validação de usuários foi utilizado o ASP.NET Identity pois já oferece
estrutura de banco e criptografia de senha.

### Acesso a dados ###
Para operações no banco de dados foi utilizado o Entity Framework.

### Criar arquivo .pdf ###
Para criar o arquivo PDF foi utilizado o pacote iTextSharp.

### Arquivo de Log ###
Em toda varredura é criado um arquivo no diretório informado tendo a data e hora como nome.

Para todo processo não encontrado uma linha é escrita seguindo o padrão:
* dd/mm/aaaa hh:mm:ss - Processo número X não encontado.

No caso de ocorrer alguma falha ao salvar o PDF também é registrado no arquivo de log uma mensagem:
* dd/mm/aaaa hh:mm:ss - Não foi possível salvar o Documento em PDF do processo X.

### Testes ###
Como exemplo, alguns testes foram implementados utilizando Roolback Transaction no cadastrar usuário.