﻿using Br.Conselhos.Avaliacao.Dados;
using Br.Conselhos.Avaliacao.Dados.Modelos;
using Br.Conselhos.Avaliacao.Dados.Repositorio;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Br.Conselhos.Avaliacao.Forms
{
    public partial class LoginForm : Form
    {
        private Usuario usuario;
        private Contexto contexto;
        private UsuarioRepositorio repositorio;

        public LoginForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;

            contexto = new Contexto();
            repositorio = new UsuarioRepositorio(contexto);
        }

        private void entrarButton_Click(object sender, EventArgs e)
        {
            usuario = repositorio.FindUser(usuarioTextBox.Text, senhaTextBox.Text);

            if (usuario == null)
            {
                loginInvalidoMsg.Text = "Usuário ou Senha inválido.";
                loginInvalidoMsg.Visible = true;
            }
            else
            {
                Close();
            }
        }
        
        private void criarUsuarioButton_Click(object sender, EventArgs e)
        {
            Usuario usuarioNovo = new Usuario(usuarioTextBox.Text);

            IdentityResult resultado = repositorio.CreateUser(usuarioNovo, senhaTextBox.Text);
            if (resultado.Succeeded)
            {
                usuario = usuarioNovo;
                Close();
            }
            else
            {
                loginInvalidoMsg.Text = resultado.Errors.ToList()[0];
                loginInvalidoMsg.Visible = true;
            }
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (usuario == null)
                Application.Exit();
        }
    }
}
