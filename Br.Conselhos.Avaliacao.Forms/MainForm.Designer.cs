﻿namespace Br.Conselhos.Avaliacao.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.diretorioTextBox = new System.Windows.Forms.TextBox();
            this.pastaBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.iniciarVarreduraButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Diretório dos arquivos";
            // 
            // diretorioTextBox
            // 
            this.diretorioTextBox.Location = new System.Drawing.Point(15, 25);
            this.diretorioTextBox.Name = "diretorioTextBox";
            this.diretorioTextBox.ReadOnly = true;
            this.diretorioTextBox.Size = new System.Drawing.Size(257, 20);
            this.diretorioTextBox.TabIndex = 1;
            this.diretorioTextBox.Text = "Dois clicks para escolher o diretório";
            this.diretorioTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.diretorioTextBox_MouseDoubleClick);
            // 
            // iniciarVarreduraButton
            // 
            this.iniciarVarreduraButton.Enabled = false;
            this.iniciarVarreduraButton.Location = new System.Drawing.Point(75, 51);
            this.iniciarVarreduraButton.Name = "iniciarVarreduraButton";
            this.iniciarVarreduraButton.Size = new System.Drawing.Size(124, 23);
            this.iniciarVarreduraButton.TabIndex = 2;
            this.iniciarVarreduraButton.Text = "Iniciar Varredura";
            this.iniciarVarreduraButton.UseVisualStyleBackColor = true;
            this.iniciarVarreduraButton.Click += new System.EventHandler(this.iniciarVarreduraButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(15, 97);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(257, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 140);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.iniciarVarreduraButton);
            this.Controls.Add(this.diretorioTextBox);
            this.Controls.Add(this.label1);
            this.Name = "MainForm";
            this.Text = "Main - BR Conselhos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox diretorioTextBox;
        private System.Windows.Forms.FolderBrowserDialog pastaBrowserDialog;
        private System.Windows.Forms.Button iniciarVarreduraButton;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

