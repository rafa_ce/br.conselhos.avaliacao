﻿namespace Br.Conselhos.Avaliacao.Forms
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.usuarioLabel = new System.Windows.Forms.Label();
            this.senhaLabel = new System.Windows.Forms.Label();
            this.usuarioTextBox = new System.Windows.Forms.TextBox();
            this.senhaTextBox = new System.Windows.Forms.TextBox();
            this.entrarButton = new System.Windows.Forms.Button();
            this.loginInvalidoMsg = new System.Windows.Forms.Label();
            this.criarUsuarioButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Faça o Login para continuar";
            // 
            // usuarioLabel
            // 
            this.usuarioLabel.AutoSize = true;
            this.usuarioLabel.Location = new System.Drawing.Point(12, 63);
            this.usuarioLabel.Name = "usuarioLabel";
            this.usuarioLabel.Size = new System.Drawing.Size(43, 13);
            this.usuarioLabel.TabIndex = 1;
            this.usuarioLabel.Text = "Usuário";
            // 
            // senhaLabel
            // 
            this.senhaLabel.AutoSize = true;
            this.senhaLabel.Location = new System.Drawing.Point(12, 97);
            this.senhaLabel.Name = "senhaLabel";
            this.senhaLabel.Size = new System.Drawing.Size(38, 13);
            this.senhaLabel.TabIndex = 2;
            this.senhaLabel.Text = "Senha";
            // 
            // usuarioTextBox
            // 
            this.usuarioTextBox.Location = new System.Drawing.Point(71, 60);
            this.usuarioTextBox.Name = "usuarioTextBox";
            this.usuarioTextBox.Size = new System.Drawing.Size(194, 20);
            this.usuarioTextBox.TabIndex = 3;
            // 
            // senhaTextBox
            // 
            this.senhaTextBox.Location = new System.Drawing.Point(71, 97);
            this.senhaTextBox.Name = "senhaTextBox";
            this.senhaTextBox.PasswordChar = '⏺';
            this.senhaTextBox.Size = new System.Drawing.Size(194, 20);
            this.senhaTextBox.TabIndex = 4;
            // 
            // entrarButton
            // 
            this.entrarButton.Location = new System.Drawing.Point(71, 138);
            this.entrarButton.Name = "entrarButton";
            this.entrarButton.Size = new System.Drawing.Size(75, 23);
            this.entrarButton.TabIndex = 5;
            this.entrarButton.Text = "Entrar";
            this.entrarButton.UseVisualStyleBackColor = true;
            this.entrarButton.Click += new System.EventHandler(this.entrarButton_Click);
            // 
            // loginInvalidoMsg
            // 
            this.loginInvalidoMsg.AutoSize = true;
            this.loginInvalidoMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.loginInvalidoMsg.ForeColor = System.Drawing.Color.Red;
            this.loginInvalidoMsg.Location = new System.Drawing.Point(11, 174);
            this.loginInvalidoMsg.Name = "loginInvalidoMsg";
            this.loginInvalidoMsg.Size = new System.Drawing.Size(0, 17);
            this.loginInvalidoMsg.TabIndex = 6;
            this.loginInvalidoMsg.Visible = false;
            // 
            // criarUsuarioButton
            // 
            this.criarUsuarioButton.Location = new System.Drawing.Point(161, 138);
            this.criarUsuarioButton.Name = "criarUsuarioButton";
            this.criarUsuarioButton.Size = new System.Drawing.Size(104, 23);
            this.criarUsuarioButton.TabIndex = 7;
            this.criarUsuarioButton.Text = "Criar usuário";
            this.criarUsuarioButton.UseVisualStyleBackColor = true;
            this.criarUsuarioButton.Click += new System.EventHandler(this.criarUsuarioButton_Click);
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 207);
            this.Controls.Add(this.criarUsuarioButton);
            this.Controls.Add(this.loginInvalidoMsg);
            this.Controls.Add(this.entrarButton);
            this.Controls.Add(this.senhaTextBox);
            this.Controls.Add(this.usuarioTextBox);
            this.Controls.Add(this.senhaLabel);
            this.Controls.Add(this.usuarioLabel);
            this.Controls.Add(this.label1);
            this.Name = "LoginForm";
            this.Text = "Login";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LoginForm_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label usuarioLabel;
        private System.Windows.Forms.Label senhaLabel;
        private System.Windows.Forms.TextBox usuarioTextBox;
        private System.Windows.Forms.TextBox senhaTextBox;
        private System.Windows.Forms.Button entrarButton;
        private System.Windows.Forms.Label loginInvalidoMsg;
        private System.Windows.Forms.Button criarUsuarioButton;
    }
}