﻿using Br.Conselhos.Avaliacao.Dados;
using Br.Conselhos.Avaliacao.Dados.Modelos;
using Br.Conselhos.Avaliacao.Dados.Repositorio;
using Br.Conselhos.Avaliacao.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Br.Conselhos.Avaliacao.Forms
{
    public partial class MainForm : Form
    {
        private ArquivoLog logger;

        LoginForm loginForm = new LoginForm();

        public MainForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loginForm.ShowDialog();
        }

        private void diretorioTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (pastaBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                diretorioTextBox.Text = pastaBrowserDialog.SelectedPath;
                iniciarVarreduraButton.Enabled = true;
            }
        }

        private void iniciarVarreduraButton_Click(object sender, EventArgs e)
        {
            FileInfo[] arquivos = GetArquivosTxt();

            progressBar1.Maximum = arquivos.Length;

            using (var contexto = new Contexto())
            {
                ProcessoRepositorio repositorio = new ProcessoRepositorio(contexto);

                string caminhoArquivoLog = Path.Combine(diretorioTextBox.Text + "\\", DateTime.Now.ToString("MMddyyyyHHmm") + ".txt");
                logger = new ArquivoLog(caminhoArquivoLog);

                ProcessarArquivos(arquivos, repositorio);

                logger.Fechar();
                contexto.SaveChanges();
            }

            MessageBox.Show("Varredura concluída.");
        }

        private FileInfo[] GetArquivosTxt()
        {
            DirectoryInfo diretorio = new DirectoryInfo(diretorioTextBox.Text);

            return diretorio.GetFiles("*.txt");
        }

        private void ProcessarArquivos(FileInfo[] arquivos, ProcessoRepositorio repositorio)
        {
            foreach (FileInfo arquivo in arquivos)
            {
                string numero = Path.GetFileNameWithoutExtension(arquivo.Name);
                Processo processo = repositorio.BuscarPeloNumero(numero);
                if (processo == null)
                {
                    logger.EscreverProcessoNaoEncontrado(numero);
                }
                else
                {
                    ConverteArquivoPDF(arquivo, processo);
                }

                progressBar1.Value += 1;
            }            
        }

        private void ConverteArquivoPDF(FileInfo arquivo, Processo processo)
        {
            try
            {
                string documentoPdf = PDFUtil.ConverteTxtEmPDF(arquivo.FullName);
                processo.Documento = System.IO.File.ReadAllBytes(documentoPdf);
            }
            catch (Exception ex)
            {
                logger.EscreverFalhaConverterPDF(processo.Numero);
            }
        }
    }
}
