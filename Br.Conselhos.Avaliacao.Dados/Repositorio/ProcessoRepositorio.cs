﻿using Br.Conselhos.Avaliacao.Dados.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Dados.Repositorio
{
    public class ProcessoRepositorio
    {
        private Contexto contexto;

        public ProcessoRepositorio(Contexto contexto)
        {
            this.contexto = contexto;
        }

        public void AdicionarProcesso(Processo processo)
        {
            contexto.Processos.Add(processo);
        }

        public Processo BuscarPeloNumero(string numero)
        {
            return contexto.Processos.FirstOrDefault(p => p.Numero.Equals(numero));
        }
    }
}
