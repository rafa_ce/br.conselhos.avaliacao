﻿using Br.Conselhos.Avaliacao.Dados.Modelos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Dados.Repositorio
{
    public class UsuarioRepositorio
    {
        private UserManager<Usuario> userManager;

        public UsuarioRepositorio(Contexto contexto)
        {
            this.userManager = new UserManager<Usuario>(new UserStore<Usuario>(contexto));
        }

        public IdentityResult CreateUser(Usuario user, string password)
        {
            return userManager.Create(user, password);
        }

        public Usuario FindUser(string userName, string password)
        {
            return userManager.Find(userName, password);
        }
    }
}
