﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Dados.Modelos
{
    public class Processo
    {
        public string Numero { get; set; }
        public byte[] Documento { get; set; }

        public Processo()
        {

        }

        public Processo(string numero)
        {
            Numero = numero;
        }
    }
}
