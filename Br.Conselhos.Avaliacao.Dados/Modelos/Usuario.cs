﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Dados.Modelos
{
    public class Usuario : IdentityUser
    {
        public Usuario() { }

        public Usuario(string userName)
        {
            UserName = userName;
        }
    }
}
