﻿using Br.Conselhos.Avaliacao.Dados.Modelos;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Br.Conselhos.Avaliacao.Dados
{
    public class Contexto : IdentityDbContext<Usuario>
    {
        public Contexto() : base("Contexto")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<Processo> Processos { get; set; }

        protected override void OnModelCreating(DbModelBuilder mb)
        {
            mb.HasDefaultSchema("public"); //para o postgreSQL
            mb.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();

            mb.Entity<Processo>().HasKey(p => p.Numero);

            base.OnModelCreating(mb);
        }

        public void MarcarObjetoEditado(object objeto)
        {
            Entry(objeto).State = EntityState.Modified;
        }
    }
}
