// <auto-generated />
namespace Br.Conselhos.Avaliacao.Dados.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class TabelaProcesso : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TabelaProcesso));
        
        string IMigrationMetadata.Id
        {
            get { return "201612171635212_TabelaProcesso"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
