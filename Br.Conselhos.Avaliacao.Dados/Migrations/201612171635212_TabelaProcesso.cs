namespace Br.Conselhos.Avaliacao.Dados.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TabelaProcesso : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.Processo",
                c => new
                    {
                        Numero = c.String(nullable: false, maxLength: 128),
                        Documento = c.Binary(),
                    })
                .PrimaryKey(t => t.Numero);
            
        }
        
        public override void Down()
        {
            DropTable("public.Processo");
        }
    }
}
