namespace Br.Conselhos.Avaliacao.Dados.Migrations
{
    using Modelos;
    using Repositorio;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Br.Conselhos.Avaliacao.Dados.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Br.Conselhos.Avaliacao.Dados.Contexto context)
        {
            UsuarioRepositorio repositorio = new UsuarioRepositorio(context);
            repositorio.CreateUser(new Usuario("Admin"), "Admin1");
            context.SaveChanges();

            ProcessoRepositorio processoRepositorio = new ProcessoRepositorio(context);
            processoRepositorio.AdicionarProcesso(new Processo("78634"));
            context.SaveChanges();
        }
    }
}
